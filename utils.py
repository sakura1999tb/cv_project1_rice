import numpy as np
import cv2
def powerlaw(img, y):
    c = np.max(img)/255
    img = np.where(c * pow(img, y) <= 255, c * pow(img, y), 255)
    return img

def fft(img):
    '' 'Fourier transform the image and return the frequency matrix after transposition' ''

    assert img.ndim == 2, 'img should be gray.'
    rows, cols = img.shape[:2]
    # Calculate the optimal size
    # According to the new size, create a new transformed image
    # Fourier transform
    fft_mat = cv2.dft(np.float32(img), flags=cv2.DFT_COMPLEX_OUTPUT)
    # Transposition, the low frequency part moves to the middle, the high frequency part moves to the surrounding
    return np.fft.fftshift(fft_mat)


def ifft(fft_mat):
    '' 'Inverse Fourier transform, return inverse transform image' ''
    # Reverse transposition, the low frequency part moves to the surrounding, the high frequency part moves to the middle
    f_ishift_mat = np.fft.ifftshift(fft_mat)
    # Inverse Fourier Transform
    img_back = cv2.idft(f_ishift_mat)
    # Convert complex number to amplitude, sqrt (re ^ 2 + im ^ 2)
    img_back = cv2.magnitude(*cv2.split(img_back))
    # Standardized to between 0 ~ 255
    cv2.normalize(img_back, img_back, 0, 255, cv2.NORM_MINMAX)
    return np.uint8(np.around(img_back))

def count_objects(img):
    contours, _ = cv2.findContours(
        img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = sorted(
        contours, key=lambda contours: cv2.boundingRect(contours)[0])
    img = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
    print(img)
    i=0
    for c in contours:
        M = cv2.moments(c)

        if M['m00'] == 0:
            M['m00'] = 1
       
        x = int(M['m10'] / M['m00'])
        y = int(M['m01'] / M['m00'])
        number = str(i + 1)
        cv2.drawContours(img, [c], 0, (0, 0, 255), 2)
        cv2.circle(img, (x, y), 4, (255,0 , 0), -1)
        cv2.putText(img, number, (x, y), 1, 1, (255, 255, 255), 1)
        i = i + 1
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img, len(contours)