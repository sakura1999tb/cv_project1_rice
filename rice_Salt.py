import cv2
from matplotlib import cm, pyplot as plt
import numpy as np
from utils import count_objects,powerlaw

## Đọc file
img = cv2.imread("./images/rice_salt.png",0)
#plt.imshow(img,cmap="gray")
#plt.show()

## Lọc nhiễu hạt tiêu
img = cv2.medianBlur(img, 3)
#plt.imshow(img,cmap="gray")
#plt.show()

## Tăng độ tương phản
img = powerlaw(img, 1.0111)
img = np.uint8(img)
#plt.imshow(img,cmap="gray")
#plt.show()

## Tăng k/c các vật thể
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
#plt.imshow(img,cmap="gray")
#plt.show()

## Khôi phục 1 phần hình dạng ban đầu
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5)))
img = np.uint8(img)
#plt.imshow(img,cmap="gray")
#plt.show()

## Lập biên
img = cv2.Canny(img, 100, 200)
#plt.imshow(img,cmap="gray")
#plt.show()

## Tăng kích thước vật thể để chống vỡ cạnh
img = cv2.dilate(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2)))
#plt.imshow(img,cmap="gray")
#plt.show()

## Đếm
img, count = count_objects(img)
print("count: ", count)
title = str(count) + " đối tượng"
plt.imshow(img,cmap="gray")
plt.title(title)
plt.show()
