import cv2
import numpy as np
from matplotlib import cm, pyplot as plt
import math
from utils import ifft, fft, count_objects, powerlaw

## Đọc file
img = cv2.imread('./images/rice.png', 0)
#plt.imshow(img,cmap="gray")
#plt.show()

## Tăng độ tương phản
img = powerlaw(img, 1.0111)
img = np.uint8(img)
#plt.imshow(img,cmap="gray")
#plt.show()

## Erode để giảm k/c vật thể
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2, 2)))
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
#plt.imshow(img,cmap="gray")
#plt.show()

## Dillate để khôi phục 1 phần hình dạng ban đầu
img = cv2.dilate(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
img = np.uint8(img)
#plt.imshow(img,cmap="gray")
#plt.show()

## Lập biên
img = cv2.Canny(img, 100, 200)
img = cv2.dilate(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (1, 2)))
#plt.imshow(img,cmap="gray")
#plt.show()

## Đếm 
img, count = count_objects(img)

title = str(count) + " đối tượng"
plt.imshow(img, cmap="gray")
plt.title(title)
plt.show()