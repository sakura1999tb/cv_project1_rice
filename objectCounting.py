﻿import cv2

# Đọc file
img = cv2.imread('./images/objets1.jpg')

# Convert to Gray
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# Cho qua bộ lọc trung vị
gray = cv2.medianBlur(gray, 3)

# Lập cạnh bằng Canny
canny = cv2.Canny(gray, 100, 50)

# Làm béo hình ảnh để tránh vỡ cạnh
canny = cv2.dilate(canny, None, iterations= 5)

#_, thres = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV)

# Đếm
contours, _ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
contours = sorted(contours, key=lambda contours: cv2.boundingRect(contours)[0])
i = 0
for c in contours:
    M = cv2.moments(c)

    if M['m00'] == 0 : M['m00'] = 1
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])

    number = str(i + 1)
    cv2.drawContours(img, [c], 0, (0, 0, 255), 2)
    cv2.circle(img, (x, y), 4, (0, 255, 0), -1)
    cv2.putText(img, number, (x, y), 2, 1, (0, 255, 255), 1)
    cv2.imshow('Result', img)
    cv2.waitKey(10)
    i = i + 1

print('Tong so object trong hinh: ', number)
cv2.waitKey(0)
cv2.destroyWindow()


