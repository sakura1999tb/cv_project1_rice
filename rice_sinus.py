from numpy.core.numeric import roll
from utils import powerlaw,fft,ifft,count_objects
import cv2
from matplotlib import cm, pyplot as plt
import numpy as np

# Đọc file
img = cv2.imread("./images/rice_sinus.png",0)

# Tăng cường độ tương phản bằng powerlaw
img = powerlaw(img, 1.011)
cv2.normalize(img, img, 0, 255, cv2.NORM_MINMAX)
img = np.uint8(img)

## Lấy ra các thông tin cần thiết cho ảnh
rows, cols = img.shape[:2]
mid_cols = int(cols/2)
mid_rows = int(rows/2)



## Nhận xét 
## Ta xét ảnh fft của ảnh gốc rice.png và ảnh đang cần xử lí rice_sinus.png
# original_img = cv2.imread("./images/rice.png",0)
# sinus_img = cv2.imread("./images/rice_sinus.png",0)
# fft_original_img = fft(original_img)
# fft_sinus_img = fft(sinus_img)
# fft_original_img = np.log(1+cv2.magnitude(fft_original_img[:,:,0], fft_original_img[:,:,1]))
# fft_sinus_img = np.log(1+cv2.magnitude(fft_sinus_img[:,:,0], fft_sinus_img[:,:,1]))
# plt.subplot(121)
# plt.imshow(fft_original_img, cmap="gray")
# plt.title("Original Image")
# plt.subplot(122)
# plt.imshow(fft_sinus_img, cmap="gray")
# plt.title("Sinus Image")
# plt.show()

######################

## Biến đổi fourier rời rạc 2 chiều
img = fft(img)

## Khử nhiễu solid bằng mặt nạ mask
mask = np.ones((rows, cols, 2), np.uint8)
mask[mid_cols-5:mid_cols+5, mid_rows-15:mid_rows-6] = 0
mask[mid_cols-5:mid_cols+5, mid_rows+6:mid_rows+15] = 0
img = img * mask

## Biểu diễn độ lớn biên độ của ảnh sau khi khử nhiễu Solid bằng mặt nạ mask
magnitude = np.log(1+cv2.magnitude(img[:, :, 0], img[:, :, 1]))

## Biến đổi fourier ngược để thu lại ảnh
img = np.fft.ifftshift(img)
img = cv2.idft(img)
img = cv2.magnitude(img[:, :, 0], img[:, :, 1])
cv2.normalize(img, img, 0, 255, cv2.NORM_MINMAX)
img = np.uint8(img)
# plt.imshow(img, cmap="gray")
# plt.title("Sinus Image")
# plt.show()

## Giảm khoảng cách giữa 2 vật thể bằng erode
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5)))
img = cv2.erode(img, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3)))
# plt.imshow(img, cmap="gray")
# plt.title("Sinus Image")
# plt.show()

## Cân bằng sáng cho background bằng MORPH_GRADIENT
kernel = np.ones((3,3), np.uint8)
img = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, kernel)
# plt.imshow(img, cmap="gray")
# plt.title("Sinus Image")
# plt.show()

## Lấy ngưỡng OTSU cho ảnh
ret3,img = cv2.threshold(img,0,255,cv2.THRESH_OTSU)
# plt.imshow(img, cmap="gray")
# plt.title("Sinus Image")
# plt.show()

## Khử nhiễu ở biên phải và biên dưới của ảnh ?????????? Cần fix
img[:,cols-7:cols-4] = cv2.erode(img[:,cols-7:cols-4], cv2.getStructuringElement(cv2.MORPH_RECT, (7, 1)))
img[:,cols-7:] = cv2.erode(img[:,cols-7:], cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5)))
img[:,cols-7:] = cv2.dilate(img[:,cols-7:], cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5)))
img[rows-10:,:] = cv2.erode(img[rows-10:,:], cv2.getStructuringElement(cv2.MORPH_RECT, (4, 7)))
img[rows-10:,:] = cv2.dilate(img[rows-10:,:], cv2.getStructuringElement(cv2.MORPH_RECT, (4, 7)))
# plt.imshow(img, cmap="gray")
# plt.title("Sinus Image")
# plt.show()

## Làm béo đối tượng cho dễ nhìn
img = cv2.dilate(img, cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5)))
plt.imshow(img, cmap="gray")
plt.title("Sinus Image")
plt.show()

## Đếm
img, count = count_objects(img)
print("count: ", count)
title = str(count) + " đối tượng"
plt.imshow(img, cmap="gray")
plt.title(title)
plt.show()
