from utils import powerlaw,count_objects
import cv2
from matplotlib import cm, pyplot as plt
import numpy as np

## Đọc file
img = cv2.imread("./images/rice_dark.png",0)
#plt.imshow(img,cmap="gray")
#plt.show()

## Tăng độ tương phản
img = powerlaw(img, 1.9999999999999)
img = np.uint8(img)
#plt.imshow(img,cmap="gray")
#plt.show()

## Lấy ngưỡng
ret,thresh1 = cv2.threshold(img,5,255,cv2.THRESH_BINARY)
#plt.imshow(img,cmap="gray")
#plt.show()

## Đếm
img, count = count_objects(thresh1)
print("count: ", count)
title = str(count) + " đối tượng"
plt.imshow(img, cmap="gray")
plt.title(title)
plt.show()
